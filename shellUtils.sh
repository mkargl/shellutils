#!/bin/bash
#
#   Copyright (C) 2011-213  Michael Kargl <michaelkargl@outlook.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# ---------------------------------------------------------------------

###### --------------- TODO -----------------
###### --------------------------------------
# none




###### ------------- IMPORTS ----------------
###### --------------------------------------
# none




###### ------------ ARGUMENTS ---------------
###### --------------------------------------
# none




###### ----------GLOBAL VARIABLES -----------
###### --------------------------------------
# none




###### ----- INTERNAL VARIABLES ---------------
###### ---only edit if you know what you do----
# none




###### ---------- FUNCTIONS -----------------
###### --------------------------------------
    
    # Takes a string and reverts it
    # Arguments:
    #   $1   Some string
    # Example:
    #   revert "hello"
    #   --> "olleh"
    # Returns:
    #   reverted String 
    function shellUtils.revert {
        local string=$1   # contains string to reverse
        local gnirts="" # contains reversed string
        
        # iterate string from back to forth
        local counter=${#string}
        while [[ $counter -gt 0 ]]; do
            gnirts="$gnirts${string:$((counter-1)):1}"
            counter=$((counter-1))
        done
        
        echo "$gnirts"
    }
    
    
    

    # Takes a string that contains special encoded characters:
    #  &lt; &gt; &nbsp;
    # and converts them back into their representive characters
    #  <     >    \n
    #
    # Arguments:
    #   $@:   Some string containing specials
    # Returns:
    #   "Decoded" string
    # Example:
    #   Input:  shellutils.special2char html text as follows &lt;body&gt;
    #   Output: "html text as follows <body>"
    function shellUtils.special2char {
        local encoded="$@"
        local decoded=""
        
        # replace encoded string with its unencoded counterpart
        decoded=${encoded//&gt;/>}
        decoded=${decoded//&lt;/<}
        decoded=${decoded//&nbsp;/\\t}
        
        echo $decoded
    }




    # Takes a string that contains unencoded special characters:
    # >, <, \n
    # and encodes them to html entities
    # &gt; &lt; &nbsp;
    #
    # Arguments:
    #   $@: Some string containing unencoded specials
    # Returns:
    #   "Encoded" string
    # Example:
    #   Input:  "html text as follows <body>"
    #   Output: shellutils.special2char html text as follows &lt;body&gt;
    function shellUtils.char2special {
       local decoded="$@"
       local encoded=""
       
       # replace character with its encoded counterpart
       encoded=${decoded//</&lt;}
       encoded=${encoded//>/&gt;}
       encoded=${encoded//\\t/&nbsp;}
     
       echo $encoded
    }
    
    
    
    
    # Converts a string to a format usable in an Internet URL
    # Parameters:
    #   $@:   String to encode
    # Example:
    #   shellUtils.urlencode "a+b (asd)__23"
    #     ->  a%2Bb%20%28asd%29__%32%33  
    # Original source:
    #   http://blogs.gnome.org/shaunm/2009/12/05/urlencode-and-urldecode-in-sh/
    function shellUtils.urlencode {
        # This is important to make sure string manipulation is handled
        # byte-by-byte.
        LANG=C
        local string="$@"
        local i=0
        local encoded=""
        
        # iterate over each letter in string
        while [ $i -lt ${#string} ]; do
            local c=${string:$i:1}

            if echo "$c" | grep -q '[a-zA-Z/:_\.\-]'; then
                # no encoding needed
                encoded="$encoded$c"

            else
                # use hexadecimal value for special characters
                local hex=$(printf "%X" "'$c'")
                encoded="$encoded%$hex"
            fi

            i=$((i+1))
        done
        
        echo "$encoded"
    }




    # Converts an already encoded string to normal, readyble, text
    # Parameters:
    #   $1:   String to decode
    # Example:
    #   shellUtils.urldecode " a%2Bb%20%28asd%29__%32%33    "
    #     ->  a+b (asd)__23
    # Original source:
    #   http://blogs.gnome.org/shaunm/2009/12/05/urlencode-and-urldecode-in-sh/
    function shellUtils.urldecode {
        local string="$@"
        local i=0
        local decoded=""

        # iterate over each letter in string
        while [ $i -lt ${#string} ]; do
            local c=${string:$i:1}
        
            # start of hexcode found
            if [ "x$c" = "x%" ]; then
                
                # convert hex back to string
                local hex=${arg:$((i+1)):2}
                str=$(printf "\x$hex")
                decoded="$decoded$str"

                i=$((i+3))
            else
                # no converting needed
                decoded="$decoded$c"
                i=$((i+1))
            fi
        done
        
        echo "$decoded"
    }




    # Takes a unix path and converts it to a
    # windows path. This is needed when using
    # cygwin under windows
    #
    # Arguments:
    #   $1:    Path in unixformat
    # Example:
    #   shellUtils.unixPath2Dos /cygdrive/c/cygwin
    #   --> C:\\cygdrive\cygwin
    function shellUtils.cygPath2Dos {
        unix_path="$1"
        declare -a file_arr=(${unix_path//\// })
        
        # harddrive
        dos_path="${file_arr[1]}:\\"
        
        # cut away drive (/cygdrive/c/)
        substr="${unix_path:12}"

        # replace / with \ and put path together
        dos_path="$dos_path${substr//\//\\}"
              
        echo "$dos_path"
    }
    
    
    
    
    # Changes the extention of given filename to another one
    # 
    # Paramter:
    #   $1:   file
    #   $2:   extention of file
    #   $3:   new extention of file
    #
    # Example:
    #   swapFileEnding files/a.txt txt doc
    #    --> files/a.doc
    #   swapFileEnding files/a.txt exe doc
    #    --> ignored because file a is no .exe
    function shellUtils.swapFileEnding {
        local file="$1"
        local extention="$2"
        local newExtention="$3"
        
        extentionLength=${#extention}
        fileLength=${#file}
        
        # only process files with wanted extention
        if [ ${file##*.} = "$extention" ]; then
            toFilename="${file:0:$((fileLength-extentionLength-2))}.$newExtention"
            mv -v "$file" "$toFilename" | tee -a log
        fi
    }
    
    
    
    
    # This method is used by the traverseDirectory method (below)
    function shellUtils.doSomething {
        file=$1
        echo "do something with file [$file]"
    }
    
    
    
    
    # Traverses a directory and swaps
    # all the file extentions of its
    # files
    #
    # Paramters:
    #   $1:   Root directory to traverse
    # Example:
    #   traverseDirectory ./Vides
    #    -> traverses the directory Videos
    #       recursively
    function shellUtils.traverseDirectory {
        local directory="$1"
        
        # iterate through directory
        fileList="fileList$RANDOM"
        ls -1 "$directory" > "$fileList"
        while read file; do
            
            file="$directory/$file"
            if [ -d "$file" ]; then
                # if entry is directory then traverse this one too
                shellUtils.traverseDirectory "$file"&
                
            elif [ -f "$file" ]; then
                # otherwise do something
                
                # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                # CHANGE THIS LINE TO SOMETHING SUITABLE FOR YOU
                # OR ADD IT TO THE doSomething() METHOD
                # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                shellUtils.doSomething "$file"
            fi
              
        done < "$fileList"
        rm "$fileList"
    }
    
    
    
    
    # Executes a command and saves the result as image on disk
    #
    # Arguments:
    #   $#-1:  Command to execute
    #   $#:    Image file to create
    # Example:
    #   Input:  shellUtils.cmd2img cat log-michi\[10-05-2013\].txt img.jpg
    #   Output: Creates a file named img.jpg with the contents of the 
    #           logfile
    function shellUtils.cmd2img {
        local cmd=""     # text which is getting written in
        local img=""     # an image file
        local arg=($@)   # converting $@ to an array ()

        if [ ! $# -gt 1 ]; then
            echo "Usage image text"
            echo "Usage cmd2text image.png"
            exit 1
        fi

        # get the index of the last element of the array
        # -1 because bash arrays have 0 based indizes
        len=$((${#arg[@]}-1))
        img=${arg[$len]}       # get last element
        cmd=${arg[@]:0:$len}   # get all except last

        # execute the command to output
        $cmd | convert label:@- $img

        # if successful = 0
        if [ $? -eq 0 ]; then
            echo "successful"
            echo "opening image"
            eog $img
        else
            # if not integer > 0
            echo "not successful"
        fi
    }
    
    
    
    
    # Logs text in a file
    # Arguments:
    #   $@:  Logtext
    # Example:
    #   Input:  shellUtils.log This is a text logentry
    #   Output: creates a file called
    #           "log-michi[10-05-2013].txt"
    #           in the current directory and inserts the line 
    #           "michi[ 10-05-2013 at 17:28 ]:  This is a text logentry"
    function shellUtils.log {
        local logtext="$@"
        local user=`whoami`
        local today=`date +"%d-%m-%Y"`
        local time=`date +"%H:%M"`
        local logfile="./log-$user[$today].txt"

        if [ -z "$logtext" ]; then
            echo "Error: Missing argument"
            echo "Usage: log this is an log entry"
            exit
        fi

        # Appends log in logfile as summary
        echo "$user[ $today at $time ]:  $logtext" >> "$logfile"
    }

    


    # Colorize text in your bash instance.
    # Arguments:
    #    $1:  Text decoration:  
    #       bold        :   --bold -b
    #       continuous  :   --continuous -c
    #       underlined  :   --underlined -u
    #       intense     :   --intense   -i
    #       regular     :   --regular   -r
    #       background  :   --background -B
    #    $2: color:
    #      red, green, blue, cyan, purple, white
    # Example:
    #    Input:  shellUtils.color yellow; echo "test"
    #    Output: Prints the word yellow in "test"
    function shellUtils.color {
        local COLOR="White"
        local COLOR_CODE=30 # Regular

        # Mode Code
        # Intense, Background,...
        local MODE_CODE=0
        # Type Code
        # Bold or Underlined
        local TYPE_CODE=0

        # argument patterns
        local PATTERN_REGULAR="\-r|--regular"
        local PATTERN_BOLD="\-b|--bold"
        local PATTERN_CONTINUOUS="\-c|--continuous"
        local PATTERN_UNDERLINED="\-u|--underlined"
        local PATTERN_INTENSE="\-i|--intense"
        local PATTERN_BACKGROUND="\-B|--background"

        # argument bits
        local REGULAR=1
        local BOLD=0
        local UNDERLINED=0
        local INTENSE=0
        local BACKGROUND=0
        local CONTINUOUS=0
        
        
        if [ $# -gt 0 ]; then
            COLOR=`echo $1 | awk '{print tolower($0)}'`
            local ARG=$(echo $@ |egrep -o "\-.+|--.+")
        else
            echo "No color defined:"
            echo "Usage: cecho red text text"
        fi

        # get the modes the text should be displayed in
        # for example: bold + underlined + yellow backgroun
        
        local sol=$(echo $ARG|egrep -c $PATTERN_REGULAR)
        if [ $sol -gt 0 ]; then
                REGULAR=1
                TYPE_CODE=0
        fi
        sol=$(echo $ARG|egrep -c $PATTERN_BOLD)
        if [ $sol -gt 0 ]; then
                BOLD=1
                TYPE_CODE=1
        fi
        sol=$(echo $ARG|egrep -c $PATTERN_UNDERLINED)
        if [ $sol -gt 0 ]; then
                UNDERLINED=1
                TYPE_CODE=4
        fi
        sol=$(echo $ARG|egrep -c $PATTERN_INTENSE)
        if [ $sol -gt 0 ]; then
                INTENSE=1
        fi
        sol=$(echo $ARG|egrep -c $PATTERN_BACKGROUND)
        if [ $sol -gt 0 ]; then
                BACKGROUND=1
        fi
        sol=$(echo $ARG|egrep -c $PATTERN_CONTINUOUS)
        if [ $sol -gt 0 ]; then
                CONTINUOUS=1
        fi


        # define the color that is used
        #
        if [ $COLOR == "red" ]      || [ $COLOR == "r" ] ; then
            COLOR_CODE=1
        elif [ $COLOR == "green" ]  || [ $COLOR == "g" ] ; then
            COLOR_CODE=2
        elif [ $COLOR == "yellow" ] || [ $COLOR == "y" ] ; then
            COLOR_CODE=3
        elif [ $COLOR == "blue" ]   || [ $COLOR == "b" ] ; then
            COLOR_CODE=4
        elif [ $COLOR == "purple" ] || [ $COLOR == "p" ] ; then
            COLOR_CODE=5
        elif [ $COLOR == "cyan" ]   || [ $COLOR == "c" ] ; then
            COLOR_CODE=6
        elif [ $COLOR == "white" ]  || [ $COLOR == "w" ] ; then
            COLOR_CODE=7
        else
            # default white
            COLOR_CODE=7
        fi


        # define the mode (as code) the text is beeing 
        # displayed in

        if [ $BACKGROUND -gt 0 ]; then
            if [ $INTENSE -gt 0 ]; then
                MODE_CODE=100
            else
                MODE_CODE=40
            fi
        elif [ $UNDERLINED -gt 0 ] || [ $REGULAR -gt 0 ] || [ $BOLD -gt 0 ]; then
            MODE_CODE=30
        elif [ $INTENSE -gt 0 ]; then
            MODE_CODE=90
        fi

        echo -ne "\e[${TYPE_CODE};$((MODE_CODE+$COLOR_CODE))m"  
    }
    
    
    
    # Crawls and downloades websites
    #
    # Arguments:
    #   $1: Url of the website to download
    #   $2: Limit amount of recursion
    # Example:
    #   Input:  shellUtils.webdl http://google.at 1
    #   Output: Downloads the website google and dives 
    #           1 level deeper in the tree
    function shellUtils.webdl {
		if [ $# -lt 2 ]; then
		   echo "Usage:"
		   echo "dlhp <url> <#iterations"
		   exit
		fi

		local url="$1"
		local iterations=$2

		wget -nd -r -l $iterations -e robots=off "$url"
	}
	
	
	
	# Creates an image of specified size and writes text onto it
	# Arguments:
	#   $1:  Width of image to create
	#   $2:  Height of image to create
	#   $3:  Text to print to img
	#   $4:  filename (note that alpha layer must be supported)
	# Example:
	#   Input:  shellUtils.txt2img 400 200 testtext test.png
	#   Output: A file called test.png with 400 width 200 height
	#           a transparent background on which is written testtext
	function shellUtils.txt2img {
	if [ $# -lt 4 ]; then
		echo "Arguments missing:"
		echo "$1: width of img to create"
		echo "$2: height of img to create"
		echo "$3: text to print to img"
		echo "$4: filename"
		exit
	fi

	local width=$1
	local height=$2
	local text=$3
	local filename=$4

	convert \
	  -size 200x30 \
	   xc:transparent \
	  -font /usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-B.ttf \
	  -fill white \
	  -pointsize 15\
	  -draw "text 5,15 ""'""$text""'" "$filename"
	  
	if [ $? = 0 ]; then
		echo "File [$filename] created"
	fi
}




# Combines two images into one. Second images simply overlaps
# the first one
# Arguments:
#   $1 : Input file 1
#   $2 : Input file 2
#   $3 : Output file
# Example:
#   Input:  shellUtils.combineImages image1.png image2.png
#           Image1 contains a blackish desktop wallpaper
#           Image2 contains new twitter feeds in white font
#   Output: A Deskop wallpaper that has imprinted twitter feeds
function shellUtils.combineImages {
	if [ $# -lt 3 ]; then
		echo "Arguments missing:"
		echo "$1: inputfile1"
		echo "$2: inputfile2"
		echo "$3: outputfile"
		exit 1
	fi
	
	fileA="$1"
	fileB="$2"
	output="$3"
	
	convert "$fileA" "$fileB" -gravity northeast -composite "$output"
}




# Prints test onto an image. It uses a padding of x:10px y:140px
# 
# Arguments:
#   $1 : Input file 1
#   $2 : test to imprint on the image
#   $3 : Output file
# Example:
#   Input:  shellUtils.annotateImage ~/desktopwallpaper twitterfeeds ~/desktopwallpaper
#           desktopwallpaper is a standard png file without extension
#   Output: A desktopwallpaper that has twitterfeeds imprinted in black 
#           with a padding of 10x140
function shellUtils.annotateImage  {
	if [ $# -lt 3 ]; then
		echo "Arguments missing:"
		echo "$1:  inputfile"
		echo "$2:  text"
		echo "$3:  outputfile"
		exit 1
	fi
	
	ifile="$1"
    text="$2"
	ofile="$3"
	
	convert "$ifile" \
	 -fill black \
	 -font /usr/share/fonts/truetype/fonts-japanese-gothic.ttf \
	 -pointsize 35 -annotate +10+140 "$text" "$ofile"
}


	
	
###### ---------- MAIN PROGRAM --------------
###### --------------------------------------
# none
